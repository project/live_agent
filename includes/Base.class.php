<?php
/**
 *   @copyright Copyright (c) 2011 Quality Unit s.r.o.
 *   @author Juraj Simon
 *   @version 1.0.0
 *
 *   Licensed under GPL2
 */

if (!class_exists('liveagent_Base')) {
    class liveagent_Base {
        const IMG_PATH = 'img/';
        const TEMPLATES_PATH = 'templates/';
        const JS_PATH = 'js/';
        const CSS_PATH = 'css/';
        	
        protected function _log($message) {
            if( WP_DEBUG === true ){
                if( is_array( $message ) || is_object( $message ) ){
                    $message = print_r( $message, true );
                }
             
               
			    watchdog('info', 'LiveAgent plugin log: %action', array('%action' => $message));
				
                echo $message;
            }
        }

        protected function isDebugMode() {
            return defined('DEBUG_MODE') && DEBUG_MODE == true;
        }

        protected function getTemplatesPath() {
            return WP_PLUGIN_DIR . '/' . LIVEAGENT_PLUGIN_NAME . '/' . self::TEMPLATES_PATH;
        }

        protected function getImgUrl() {
            return WP_PLUGIN_URL . '/' . LIVEAGENT_PLUGIN_NAME . '/' . self::IMG_PATH;
        }

        protected function getJsUrl() {
            return WP_PLUGIN_URL . '/' . LIVEAGENT_PLUGIN_NAME . '/' . self::JS_PATH;
        }

        protected function getCssUrl() {
            return WP_PLUGIN_URL . '/' . LIVEAGENT_PLUGIN_NAME . '/' . self::CSS_PATH;
        }

        protected function showAdminError($error) {
            $this->_log($error);
        }

        protected function showConnectionError() {           
			drupal_set_message(t("Unable to connect to Live Agent. please check your connection settings"), 'error');
        }

        public function getRemoteTrackJsUrl() {
            return variable_get('live_agent_url','') . '/scripts/trackjs.php';
        }

        public function getRemotePixUrl() {
            return variable_get('live_agent_url','') . '/scripts/pix.gif';
        }
		
        public function getRemoteApiUrl() {
            return variable_get('live_agent_url','') . '/api/index.php';
        }

        protected function isEmpty($var) {
            return $var=== null || $var=='';
        }
    }
}

?>