<?php
/**
 *   @copyright Copyright (c) 2007 Quality Unit s.r.o.
 *   @author Juraj Simon
 *   @version 1.0.0
 *
 *   Licensed under GPL2
 */

 
module_load_include('php', 'live_agent', 'includes/Buttons.class');

 
class liveagent_Settings {
    const CACHE_VALIDITY = 600;

    //internal settings
    const OWNER_SESSIONID = 'la_settings_owner-sessionid';
    const OWNER_AUTHTOKEN = 'la_settings_owner-authtoken';
    const BUTTONS_DATA = 'la_settings_buttonsdata';

    //general page
    const GENERAL_SETTINGS_PAGE_NAME = 'la_config-general-page';

    const LA_URL_SETTING_NAME = 'la_url';
    const LA_OWNER_EMAIL_SETTING_NAME = 'la_owner-email';
    const LA_OWNER_PASSWORD_SETTING_NAME = 'la_owner-password';
    const GENERAL_SETTINGS_PAGE_STATE_SETTING_NAME = 'general-settings-state';

    //buttons options
    const BUTTONS_SETTINGS_PAGE_NAME = 'la_config-buttons-page';
    const BUTTONS_CONFIGURATION_SETTING_NAME = 'la_buttons-configuration';

    const NO_AUTH_TOKEN = 'no_auth_token';

    public function initSettings() {
        register_setting(self::GENERAL_SETTINGS_PAGE_NAME, self::LA_URL_SETTING_NAME, array($this, 'sanitizeUrl'));
        register_setting(self::GENERAL_SETTINGS_PAGE_NAME, self::LA_OWNER_EMAIL_SETTING_NAME);
        register_setting(self::GENERAL_SETTINGS_PAGE_NAME, self::LA_OWNER_PASSWORD_SETTING_NAME);
        register_setting(self::BUTTONS_SETTINGS_PAGE_NAME, self::BUTTONS_CONFIGURATION_SETTING_NAME);
    }

    public function sanitizeUrl($url) {
        if (stripos($url, 'http://')!==false || stripos($url, 'https://')!==false) {
            return $url;
        }
        return 'http://' . $url;
    }

    public function clearCache() {
        update_option(self::OWNER_SESSIONID, '');
        update_option(self::OWNER_AUTHTOKEN, '');
        update_option(self::BUTTONS_DATA, '');
    }

    private function setCachedSetting($code, $value) {
        
		//$settings = get_option($code);
		$settings = variable_get($code,'');
		
        $settingValue = $value . "||" . time();
        if ($settings != '') {
            //update_option($code, $settingValue);
			variable_set($code, $settingValue);
        } else {
            //add_option($code, $settingValue);
			// pepo
			variable_set($code, $settingValue);
        }
		
    }

    private function getCachedSetting($code) {
		
        //$settings = get_option($code);
		$settings = variable_get($code,'');
        if ($settings == null || trim($settings) == '') {
            throw new liveagent_Exception_SettingNotValid((sprintf('Setting %s not defined yet.', $code)));
        }
        $settings = explode("||", $settings, 2);
        $validTo = $settings[1] + self::CACHE_VALIDITY + 0;
        if ($validTo > time()) {
            return $settings[0];
        } else {
            throw new liveagent_Exception_SettingNotValid((sprintf('Setting\'s %s validity exceeded: %s', $code, $settings['time'])));
        }
    }

    public function getOwnerSessionId() {
        try {
            return $this->getCachedSetting(self::OWNER_SESSIONID);
        } catch (liveagent_Exception_SettingNotValid $e) {
            return $this->login();
        }
    }

    public function getOwnerAuthToken() {
        try {
			
            return $this->getCachedSetting(self::OWNER_AUTHTOKEN);
        } catch (liveagent_Exception_SettingNotValid $e) {
			
            $this->login();
        }
        try {
			
            return $this->getCachedSetting(self::OWNER_AUTHTOKEN);
        } catch (liveagent_Exception_SettingNotValid $e) {
			
            $this->setCachedSetting(self::OWNER_AUTHTOKEN, self::NO_AUTH_TOKEN);
            return self::NO_AUTH_TOKEN;
        }
    }

    private function login() {
        $auth = new liveagent_Auth();
        $loginData = $auth->LoginAndGetLoginData();
        try {
            $sessionId = $loginData->getValue('session');
            $this->setCachedSetting(self::OWNER_SESSIONID, $sessionId);
        } catch (La_Data_RecordSetNoRowException $e) {
            //throw new liveagent_Exception_ConnectProblem();
			drupal_set_message(t("Unable to login:  %error", array('%error' => $e->getMessage())), 'error');
        }
        try {
            $this->setCachedSetting(self::OWNER_AUTHTOKEN, $loginData->getValue('authtoken'));
        } catch (La_Data_RecordSetNoRowException $e) {
            // we are communicating with older LA that does not send auth token
            $this->setCachedSetting(self::OWNER_AUTHTOKEN, self::NO_AUTH_TOKEN);
        }
        return $sessionId;
    }

    public function settingsDefinedForConnection() {
        return strlen(trim($this->getLiveAgentUrl())) && strlen(trim($this->getOwnerEmail()));
    }

    public function getButtonsGridRecordset() {
        try {
            $data = unserialize($this->getCachedSetting(self::BUTTONS_DATA));
            return $data;
        } catch (liveagent_Exception_SettingNotValid $e) {
            $buttonsHelper = new liveagent_helper_Buttons();
            $data = $buttonsHelper->getData();
            $this->setCachedSetting(self::BUTTONS_DATA, serialize($data));
        }
        return $data;
    }

    public function getLiveAgentUrl() {
        return variable_get('live_agent_url','');
    }

    public function getOwnerEmail() {
        return variable_get('live_agent_username','');
    }

    public function getOwnerPassword() {
        return variable_get('live_agent_password', '');
    }

    public function buttonIsEnabled($buttonId) {
       // $value = get_option(liveagent_Settings::BUTTONS_CONFIGURATION_SETTING_NAME);
        if ($value == '' || $value === null) {
            return false;
        }
        if (array_key_exists($buttonId, $value) && $value[$buttonId] == 'true') {
            return true;
        }
        return false;
    }
}

?>