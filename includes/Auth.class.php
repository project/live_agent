<?php
/**
 *   @copyright Copyright (c) 2011 Quality Unit s.r.o.
 *   @author Juraj Simon
 *   @version 1.0.0
 *
 *   Licensed under GPL2
 */
	
	
	
if (!class_exists('liveagent_Auth')) {

	module_load_include('php', 'live_agent', 'includes/Base.class');
	module_load_include('php', 'live_agent', 'includes/ConnectProblem.class');	
	module_load_include('php', 'live_agent', 'includes/Settings.class');	
	module_load_include('php', 'live_agent', 'includes/SettingNotValid.class');	
	
	class liveagent_Auth extends liveagent_Base {
		public function ping() {
			$request = new La_Rpc_DataRequest("Gpf_Common_ConnectionUtil", "ping");
			$request->setUrl($this->getRemoteApiUrl());

			try {
				$request->sendNow();
			} catch (Exception $e) {
				 drupal_set_message(t("Connection problem:  %error", array('%error' => $e->getMessage())), 'error');
				throw new liveagent_Exception_ConnectProblem();
			}
			$data = $request->getData();
			if ($data->getParam('status') != 'OK') {
				throw new liveagent_Exception_ConnectProblem();
			}
		}

		/**
		 * @return La_Rpc_Data
		 */
		public function LoginAndGetLoginData() {			
		
			$settings = new liveagent_Settings();
			
			$request = new La_Rpc_DataRequest("Gpf_Api_AuthService", "authenticate");

			$request->setField('username' ,$settings->getOwnerEmail());
			$request->setField('password' ,$settings->getOwnerPassword());
			$request->setUrl($this->getRemoteApiUrl());

			try {
				$request->sendNow();
			} catch (Exception $e) {
				drupal_set_message(t("Unable to login:  %error", array('%error' => $e->getMessage())), 'error');
			   
			}
		    return $request->getData();
		}
		
		public function getRemoteApiUrl() {
            return variable_get('live_agent_url','') . '/api/index.php';
        }
		
	}
}
?>