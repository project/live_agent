<?php
/**
 *   @copyright Copyright (c) 2011 Quality Unit s.r.o.
 *   @author Juraj Simon
 *   @version 1.0.0
 *
 *   Licensed under GPL2
 */

set_exception_handler('exception_handler');
 
class liveagent_Exception_ConnectProblem extends Exception {

	public function __construct($message = 'Connection Problem') {
            
    }
	
}

function exception_handler($exception='') {
  print t("Uncaught exception:") .  $exception->getMessage() . ' ' . $exception . "\n";
}




?>